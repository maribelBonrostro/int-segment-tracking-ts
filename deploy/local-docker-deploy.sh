#!/bin/bash

# define ROLE_ARN in ~/.env
# ROLE_ARN=arn:aws:iam::<YOUR_ACCOUNT>:role/MMTeamRole-MMPay

cd `dirname $0`
source ~/.env
CREDS=$(aws sts assume-role --role-arn $ROLE_ARN --role-session-name sls-session --out json)
export AWS_ACCESS_KEY_ID=$(echo $CREDS | jq -r '.Credentials.AccessKeyId')
export AWS_SECRET_ACCESS_KEY=$(echo $CREDS | jq -r '.Credentials.SecretAccessKey')
export AWS_SESSION_TOKEN=$(echo $CREDS | jq -r '.Credentials.SessionToken')
export AWS_ACCOUNT_ID=$(aws sts get-caller-identity --output text --query 'Account')
export HOSTED_ZONE_ID=$HOSTED_ZONE_ID
export BUILD_NUMBER=0
docker-compose build serverless
docker-compose run --rm serverless deploy --stage local --force

# TO CREATE THE CUSTOM DOMAIN
# docker-compose run --rm serverless create_domain --stage local --force
