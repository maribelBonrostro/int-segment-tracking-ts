/*
 *  Copyright (c) Message4U Pty Ltd 2014-2020
 *
 *  Except as otherwise permitted by the Copyright Act 1967 (Cth) (as amended from time to time)
 *  and/or any other applicable copyright legislation, the material may not be reproduced in any
 *  format and in any way whatsoever without the prior written consent of the copyright owner.
 */
module.exports = () => ({
  // eslint-disable-next-line global-require
  version: require('./package.json').version,
  dev: {
    messagemedia_domain: 'api.stg.messagemedia.com',
    segment: 's0QMj0DCm0zlEm7EMLXDCFtnKIH5Wb57',
    warmupConfig: {
      cronExpression: 'cron(0/55 * ? * MON-FRI *)', // Every 55 minutes Monday-Friday
      concurrency: '1',
    },
  },
  qa: {
    messagemedia_domain: 'api.stg.messagemedia.com',
    segment: 's0QMj0DCm0zlEm7EMLXDCFtnKIH5Wb57',
    warmupConfig: {
      cronExpression: 'cron(0/30 * ? * MON-FRI *)', // Every 55 minutes Monday-Friday
      concurrency: '1',
    },
  },
  pro: {
    messagemedia_domain: 'api.messagemedia.com',
    segment: 's0QMj0DCm0zlEm7EMLXDCFtnKIH5Wb57',
    warmupConfig: {
      cronExpression: 'cron(0/10 * ? * * *)', // Every 10 minutes
      concurrency: '1',
    },
  },
  local: {
    messagemedia_domain: 'api.stg.messagemedia.com',
    segment: 's0QMj0DCm0zlEm7EMLXDCFtnKIH5Wb57',
    warmupConfig: {
      cronExpression: 'cron(0/55 * ? * MON-FRI *)', // Every 55 minutes Monday-Friday
      concurrency: '1',
    },
  },
});
