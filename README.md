# Segment Tracking

 Improve data analytics

## UNIFIED SQS MESSAGE BODY

```
{
    "vendorId": "MessageMedia",
    "accountId": "SomeAccount_XXX_0001",
    "event": "message-sent",
    "properties": {
        "message":{
            "from": "workflow-message",
            "contacts": "1",
            "type": "postpaid", // postpaid or prepaid
        },
        "platform": {
          "source": "activecampaign"
          "metadata": {
            ... // Meta Data from SMS Message
          }
        }
    }
}

```


## Environment variable

 `~/.env`

SEGMENT=SEGMENT_WRITE_KEY
MESSAGEMEDIA_DOMAIN=xxxxxxx
MESSAGEMEDIA_CREDS_USERNAME=xxxxx
MESSAGEMEDIA_CREDS_PASSWORD=xxxxx

## Testing

Unit tests have been written in [Jest](https://jestjs.io/), you can run the tests from the CLI.

* Test setup is still WIP for the MVP *

```
npm run test
```

```
npm run test --coverage
```

## Lint

Linting has been setup with eslint using the airbnb rules and jest rules for testing.

```
npm run lint
```

## DEPLOY

bash deploy/local-docker-deploy.sh
```
