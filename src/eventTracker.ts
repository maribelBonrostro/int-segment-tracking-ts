import { SQSHandler, SQSEvent } from 'aws-lambda';
import 'source-map-support/register';
import Analytics from 'analytics-node';
import { getBillingType, generateHash, toAnalyticsFormat } from './lib/helpers';

const handler: SQSHandler = async (event: SQSEvent) => {
  const analytics = new Analytics(process.env.SEGMENT);
  try {
    for (const record of event.Records) {
      const messageBody = record.body ? JSON.parse(record.body) : undefined;

      if (messageBody && messageBody.vendorId && messageBody.accountId) {
        const { accountId, vendorId } = messageBody;
        /* eslint-disable @typescript-eslint/camelcase */
        const { billing_type } = await getBillingType(accountId);
        const hashUserId = generateHash(accountId, vendorId);
        /* eslint-disable @typescript-eslint/camelcase */
        const analyticsData = toAnalyticsFormat(messageBody, billing_type, hashUserId);
        console.log('analyticsData', analyticsData);

        // segment tracking
        await new Promise((resolve, reject) => {
          analytics.track(analyticsData, (err: { message: string }) => {
            if (err) {
              console.log('Track Error: ', err.message);
              return reject(err);
            }
            console.log('success');
            resolve('');
          });
        });

        await new Promise((resolve, reject) => {
          analytics.flush((err: { message: string }) => {
            if (err) {
              console.log('Flush Error: ', err.message);
              return reject(err);
            }
            console.log('Flushed, and now this program can exit!');
            resolve('');
          });
        });
      }
    }
  } catch (error) {
    console.log(error);
  }
};

export default handler;
