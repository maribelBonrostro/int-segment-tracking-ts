const AWS = require('aws-sdk');

const sqs = new AWS.SQS({
  region: 'ap-southeast-2',
});
const params = {
  QueueUrl: 'https://sqs.ap-southeast-2.amazonaws.com/558723829440/int-segment-tracking-typescript-dev-queue.fifo',
  MessageGroupId: 'MessageGroupId',
  MessageDeduplicationId: 'MessageGroupId',
  MessageBody: JSON.stringify({
    vendorId: 'MessageMedia',
    accountId: 'EcosystemSTG_AKT_0018',
    event: 'message-sent',
    properties: {
      message: {
        from: 'workflow-message',
        contacts: '1',
        type: 'sms',
      },
      platform: {
        source: 'activecampaign',
        metadata: {},
      },
    },
  }),
};
sqs.sendMessage(params, (err, data) => {
  if (err) console.log(err, err.stack); // an error occurred
  else console.log(data); // successful response
});
