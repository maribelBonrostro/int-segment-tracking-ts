import { createHash } from 'crypto';
import axios from 'axios';

export type AccountType = 'POSTPAID' | 'PREPAID';

interface MessageBody {
  vendorId: string;
  event: string;
  properties: Properties;
}

interface Properties {
  account: {
    vendor: string;
    type: AccountType;
  };
  message?: unknown;
  platform?: unknown;
}

interface AnalyticsFormat {
  userId: string;
  event: string;
  properties: Properties;
}

export const toAnalyticsFormat = (
  messageBody: MessageBody,
  billingType: AccountType,
  hashUserId: string,
): AnalyticsFormat => {
  const { properties, event, vendorId } = messageBody;
  const { message, platform } = properties;

  return {
    userId: hashUserId,
    event,
    properties: {
      account: {
        vendor: vendorId,
        type: billingType,
      },
      message,
      platform,
    },
  };
};

export const getBillingType = (accountId: string): Promise<{ billing_type: AccountType }> => {
  const { MESSAGEMEDIA_DOMAIN, MESSAGEMEDIA_CREDS_USERNAME, MESSAGEMEDIA_CREDS_PASSWORD } = process.env;

  return axios
    .request({
      method: 'GET',
      auth: { username: MESSAGEMEDIA_CREDS_USERNAME, password: MESSAGEMEDIA_CREDS_PASSWORD },
      url: `https://${MESSAGEMEDIA_DOMAIN}/v1/iam/accounts/${accountId}`,
    })
    .then(({ data }) => data);
};

export const generateHash = (vendorId: string, accountId: string): string =>
  createHash('md5').update(`${vendorId}:${accountId}`).digest('hex');
